package pepper

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Client struct {
	Host          string
	Port          string
	Username      string
	Password      string
	Debug         bool
	SkipSSLVerify bool
	HttpClient    *http.Client
	AuthToken     string
}

type Lowstate struct {
	Client   string            `json:"client"`
	Target   string            `json:"tgt"`
	Function string            `json:"fun"`
	Arg      map[string]string `json:"arg,omitempty"`
	Kwarg    map[string]string `json:"kwarg,omitempty"`
	ExprForm string            `json:"expr_form,omitempty"`
	Timeout  string            `json:"timeout,omitempty"`
	Return   string            `json:"ret,omitempty"`
}

func NewClient(options ...func(*Client)) (*Client, error) {

	c := Client{Debug: false, SkipSSLVerify: true}
	for _, option := range options {
		option(&c)
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: c.SkipSSLVerify,
		},
	}

	c.HttpClient = &http.Client{Transport: tr}
	err := c.authenticate()

	return &c, err
}

func (c *Client) authenticate() error {

	url := fmt.Sprintf("https://%s:%s/login", c.Host, c.Port)
	data := fmt.Sprintf(`{ "username":"%s", "password":"%s", "eauth": "pam" }`, c.Username, c.Password)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer([]byte(data)))
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/json")
	resp, err := c.HttpClient.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != 200 {
		return errors.New("failed to authenticate")
	}

	c.AuthToken = resp.Header.Get("X-Auth-Token")
	return nil
}

func (c *Client) request(ctx context.Context, path string, low Lowstate) (*http.Response, error) {

	url := fmt.Sprintf("https://%s:%s%s", c.Host, c.Port, path)

	l, err := json.Marshal(low)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(l))
	if err != nil {
		return nil, err
	}

	req = req.WithContext(ctx)

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Auth-Token", c.AuthToken)

	resp, err := c.HttpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, errors.New("failed to make request")
	}

	return resp, nil
}

func (c *Client) low(ctx context.Context, low Lowstate, path string) (*http.Response, error) {

	resp, err := c.request(ctx, path, low)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (c *Client) Local(ctx context.Context, low Lowstate) (string, error) {

	low.Client = "local"

	resp, err := c.low(ctx, low, "/")

	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)

	return string(data[:]), nil
}

func Host(host string) func(*Client) {
	return func(c *Client) {
		c.Host = host
	}
}

func Port(port string) func(*Client) {
	return func(c *Client) {
		c.Port = port
	}
}

func Username(username string) func(*Client) {
	return func(c *Client) {
		c.Username = username
	}
}

func Password(password string) func(*Client) {
	return func(c *Client) {
		c.Password = password
	}
}

func Debug(debug bool) func(*Client) {
	return func(c *Client) {
		c.Debug = debug
	}
}

func SkipSSLVerify(skip bool) func(*Client) {
	return func(c *Client) {
		c.SkipSSLVerify = skip
	}
}
